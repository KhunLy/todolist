﻿using Exercice.ToDoList.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ToolBox.MVVM.Base;
using ToolBox.MVVM.Commands;

namespace Exercice.ToDoList.ViewModels
{
    class MainViewModel: BindableBase
    {
        private Tache entry;

        public Tache Entry
        {
            get { return entry; }
            set { entry = value; OnPropertyChanged(); AddCommand.OnCanExecuteChanged(); }
        }

        // on utilisera des observable collections à la place des listes
        private ObservableCollection<Tache> list;

        public ObservableCollection<Tache> List
        {
            get { return list; }
            set { list = value; OnPropertyChanged(); }
        }

        public CommandBase AddCommand { get; set; }

        public MainViewModel()
        {
            AddCommand = new CommandBase(Add/*, CanAdd*/);
            List = new ObservableCollection<Tache>();
            Entry = new Tache();
        }

        private bool CanAdd()
        {
            return !string.IsNullOrEmpty(Entry.Intitule);
        }

        private void Add()
        {
            List.Add(Entry);
            Entry = new Tache();
        }

    }
}
