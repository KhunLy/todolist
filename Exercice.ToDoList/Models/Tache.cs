﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice.ToDoList.Models
{
    class Tache
    {
        public string Intitule { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public bool EstImportant { get; set; }
    }
}
